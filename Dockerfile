FROM node:10.23.0-alpine

WORKDIR /app

COPY . .

RUN npm install

RUN npm install sequelize-cli -g

ENV DB_HOST=dumbflix-db
ENV DB_USERNAME=irwanpanai
ENV DB_PASSWORD=irwanpan41

RUN sequelize db:create

COPY migrations /migrations

RUN sequelize db:migrate

EXPOSE 5000

CMD [ "npm", "start" ]
